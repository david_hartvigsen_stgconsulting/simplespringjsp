package com.iskj.controllers;

import com.iskj.global.beans.GlobalCache;
import com.iskj.services.MyTestService;
import com.iskj.session.UserSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.json.MappingJacksonJsonView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * User: skjenco
 * Date: 11/28/13
 * Time: 1:15 AM
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class MyTestController {

    private View jsonView = new MappingJacksonJsonView();

    @Autowired
    GlobalCache globalCache;

    @Autowired
    UserSession userSession;

    @Autowired MyTestService myTestService;


    @RequestMapping("myHello.htm")
    public String myTest3(ModelMap model){
        try{
            model.addAttribute("message", myTestService.test("Some Call to a Service"));
            model.addAttribute("GlobalCount", globalCache.getCount());
            model.addAttribute("SessionCount", userSession.getCount());
            userSession.addToSessionStuff(new Date().toString());
            for(String str : userSession.getSessionStuff()){
                System.out.println(str);
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return "hello";
    }

    @RequestMapping("mytest.json")
    public View myTest(Model model){
        List<Test> listOfTest = new ArrayList<>();
        Test test = new Test();
        test.setName("Stephen");
        test.setPhone("8383838");
        listOfTest.add(test);
        Test test2 = new Test();
        test2.setName("Karen");
        test2.setPhone("324234");
        listOfTest.add(test2);
        model.addAttribute("mylist",listOfTest);
        return jsonView;
    }

    @SuppressWarnings("unused WeakerAccess")
    static class Test {
        private String name;
        private String phone;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }
    }

}
