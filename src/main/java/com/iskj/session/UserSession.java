package com.iskj.session;

import java.util.ArrayList;
import java.util.List;

public class UserSession {

    private List<String> sessionStuff;
    private Integer count = 0;

    public List<String> getSessionStuff() {
        return sessionStuff;
    }

    public void addToSessionStuff(String stuff) {
        if(sessionStuff==null){
            sessionStuff = new ArrayList<>();
        }
        sessionStuff.add(stuff);
    }

    public Integer getCount() {
        count++;
        return count;
    }
}
