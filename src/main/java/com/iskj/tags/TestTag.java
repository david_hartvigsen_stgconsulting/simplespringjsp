package com.iskj.tags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

/**
 * Created by skjenco on 3/29/14.
 */
public class TestTag extends SimpleTagSupport {
    private String name;

    @Override
    public void doTag() throws JspException, IOException {
        super.doTag();
        getJspContext().getOut().print("Hello: " + name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
