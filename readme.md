* Simple JSP only example:
*   Purpose:
    To be simple not comprehensive. Configuration of Monolith Spring can feel like a lot.  To demonstrate a simple monolith Spring JSP application, I provided no Database or Security configuration.  This demonstrates basic JSP functionality. It is succinct as possible in order to be as clear as possible. 
*   Demonstrates the following technologies
    *   Spring
        *   Application Scope Configuration
        *   View Scope Configuration
        *   Controller Layer
        *   Service Layer
        *   Injection
    *   Custom Tags 
    *   JSTL
        *   Internationalization (FMT)
        *   Core
    *   Model sending response
    *   Thinking in Scopes (Not being RESTful)
        *   Global
        *   Session
    *   
*   Examples
    *   http://localhost:8080/mytest.json
        *   To view json output
    *   http://localhost:8080/myHello.htm
        *   JSP page   
* TODO 
    *   You will need to standup a tomcat server
        *   Lots of ways to do this.  Best way for a developer would be an instance integrated into your IDE. 
    *   mvn clean package.
        *   Will build your war file.
        
        
*   Good first assignment 
    *   Using a very old version of Spring.  Upgrade to your current version.     
